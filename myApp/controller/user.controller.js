var db = require('../db');
const shortid = require('shortid');

module.exports = {
  index: (req, res) => {
    res.render('search', { users: db.get('users').value() });
  },
  search: (req, res) => {
    var name_search = req.query.name;
    console.log('name_search: ' + name_search);
    let users = db.get('users').value();
    console.log(users);
    var result = users.filter(user => {
      return user.name.toLowerCase().indexOf(name_search.toLowerCase()) !== -1;
    });

    res.render('search', {
      users: result,
      ns: name_search,
    });
  },
  create: (req, res) => {
    res.render('create');
  },
};

module.exports.id = (req, res) => {
  let id = req.params.id;

  let user = db
    .get('users')
    .find({ id: id })
    .value();

  console.log(user);
  res.render('viewUser', {
    user: user,
  });
};

module.exports.postCreate = (req, res) => {
  req.body.id = shortid.generate();
  db.get('users')
    .push(req.body)
    .write();
  res.redirect('/user');
};
