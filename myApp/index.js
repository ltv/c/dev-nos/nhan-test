const express = require('express');
const path = require('path');
const app = new express();
const bodyParser = require('body-parser');
const userRoute = require('./routes/user.route');


port = 3000;
app.listen(port, () => console.log('myapp listening on port' + port));

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(express.static('./public'));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static('./views/styles'));
const myLogger = function(req, res, next){
  console.log("LOGGER");
  next();
};

app.use(myLogger);
app.get('/', function(req, res) {
  res.render('index', {
    title: 'Hey',
    message: 'Hello there!',
    name: 'Nhan',
  });
});

app.use('/user', userRoute);
