module.exports.postCreate = function (req, res, next) {

    let errors = [];
    if (!req.body.name) {
        errors.push('Bạn chưa điền tên!');
    }

    if (!req.body.phone) {
        errors.push('Bạn chưa điền số điện thoại!');
    }
    if (errors.length) {
        res.render('create', {
            errors: errors,
        });
        console.log(errors);
        return;
    }
    next()

};